const path = require('path')
const webpack = require('webpack')
const pkg = require('./package.json')

const isProduction = process.env.NODE_ENV === 'production'
const DEV_SERVER_PORT = 3001

const commonPlugins = [
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        }
    }),
    new webpack.optimize.CommonsChunkPlugin({
        names: ['common', 'vendor'],
        // in how many bundles a component needs to appear to be separated into the common bundle
        // 2 - if it appears more than once, separate it
        minChunks: 2
    })
]
const plugins = commonPlugins.concat(isProduction ? [
    // production plugins
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin()
] : [
    // development plugins
    new webpack.HotModuleReplacementPlugin()
])

module.exports = {
    entry: {
        vendor: [
            `webpack-dev-server/client?http://localhost:${DEV_SERVER_PORT}`,
            'webpack/hot/only-dev-server',
            ...Object.keys(pkg.dependencies)
        ],
        // The rest of the bundles stay as before
        todos: './app/modules/todos'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../backend/todos/static/assets'),
        publicPath: isProduction ? '/static/assets' : `http://localhost:${DEV_SERVER_PORT}/`
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                ]
            }
        ]
    },
    plugins,
    devServer: {
        host: 'localhost',
        port: DEV_SERVER_PORT,
        hot: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    }
}

