import React from 'react'
import TodoList from './TodoList'

export default class TodoListRoot extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            todos: props.initialData.todos || []
        }
    }

    render() {
        const { todos } = this.state
        return (
            <TodoList todos={todos} />
        )
    }
}
