import React from 'react'
import Todo from './components/Todo'

export default function TodoList({ todos }) {
    return (
        <div>
            {todos.length > 0 ? (
                <ul>
                    {todos.map(todo => (
                        <Todo key={todo.id} todo={todo} />
                    ))}
                </ul>
            ) : (
                <p>No things to do</p>
            )}
        </div>
    );
}
