import React from 'react'

export default function Todo({ todo: { text } }) {
    return (
        <li>{text}</li>
    )
}
