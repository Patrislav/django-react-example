import React from 'react'
import ReactDOM from 'react-dom'
import TodoList from './TodoList'

const registeredComponents = {
    TodoList
}

document.querySelectorAll('[data-component]').forEach(node => {
    const Component = registeredComponents[node.getAttribute('data-component')]
    ReactDOM.render(<Component initialData={window.INITIAL_DATA} />, node)
})

if (module.hot) {
    module.hot.accept()
}
