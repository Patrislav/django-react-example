from django.shortcuts import render

from .models import Todo

def index(request):
    # latest_todos = Todo.objects.order_by('-pub_date')[:5]
    latest_todos = [
        Todo(text = "Todo 1"),
        Todo(text = "Todo 2"),
        Todo(text = "Todo 3")
    ]
    context = { 'latest_todos': latest_todos }
    return render(request, 'todos/index.html')
